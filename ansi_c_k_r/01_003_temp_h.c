#include <stdio.h>

/* print Fahrenheit-Celsius table
for fahr = 0, 20, ..., 300; floating-point version*/

int main(int argc, char const *argv[])
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;   /* lower limit of temperature scale */
    upper = 300; /* upper limit */
    step = 20;   /* step size */

    fahr = lower;
    printf("%s\n", "Fahrenheit-Celsius Table");
    printf("%s\t%s\n", "Fahrenheit", "Celsius");
    while (fahr <= upper)
    {
        celsius = 5 * (fahr - 32) / 9;
        printf("%8.0f\t%7.1f\n", fahr, celsius);
        fahr = fahr + step;
    }
    return 0;
}