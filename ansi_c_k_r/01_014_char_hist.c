#include <stdio.h>
#include <math.h>

/* Print a histogram of frequencies of characters in its input */
int main(int argc, char const *argv[])
{
    int c, i, j, cfreq[128];

    for (i = 0; i < 128; i++)
        cfreq[i] = 0;

    while ((c = getchar()) != EOF)
        cfreq[c]++;

    int max_freq = 0;
    for (i = 0; i < 100; i++)
    {
        if (cfreq[i])
            cfreq[i] = (int)(log10(cfreq[i]) / log10(1.2));
        if (cfreq[i] > max_freq)
            max_freq = cfreq[i];
    }
    // Horizontal Histogram
    printf("Horizontal Histogram of length of words\n");
    for (i = 0; i < 100; i++)
        if (cfreq[i])
        {
            printf("%4d: ", i);
            for (j = 0; j < cfreq[i]; j++)
                printf("* ");
            printf("\n");
        }

    // Vertical Histogram
    printf("Vertical Histogram of length of words\n");
    for (j = max_freq; j >= 0; j--)
    {
        for (i = 0; i < 100; i++)
        {
            if (cfreq[i])
            {
                if (cfreq[i] <= j)
                    printf("%4s", " ");
                else
                    printf("%4s", "*");
            }
        }
        printf("\n");
    }
    for (i = 0; i < 100; i++)
        if (cfreq[i])
            printf("%4d", i);
    printf("\n");
    return 0;
}
