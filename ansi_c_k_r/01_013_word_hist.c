#include <stdio.h>

#define IN 1  /* inside a word */
#define OUT 0 /* outside a word */

/* Print a histogram of the lengths of words in its input */
int main(int argc, char const *argv[])
{
    int c, i, j, wfreq[100], freq, state;

    state = OUT;
    freq = 0;
    for (i = 0; i < 100; i++)
        wfreq[i] = 0;

    while ((c = getchar()) != EOF)
        if (c == ' ' || c == '\n' || c == '\t' || c == '.' || c == '!' || c == '?' || c == ',' || c == ';' || c == '\"')
        {
            wfreq[freq - 1]++;
            state = OUT;
        }
        else if (state == OUT)
        {
            freq = 1;
            state = IN;
        }
        else
            freq++;

    // Horizontal Histogram
    printf("Horizontal Histogram of length of words\n");
    for (i = 0; i < 100; i++)
        if (wfreq[i])
        {
            printf("%4d: ", i + 1);
            for (j = 0; j < wfreq[i]; j++)
                printf("*");
            printf("\n");
        }

    // Vertical Histogram
    printf("Vertical Histogram of length of words\n");
    int max_freq = 0;
    for (i = 0; i < 100; i++)
        if (wfreq[i] > max_freq)
            max_freq = wfreq[i];

    for (j = max_freq; j >= 0; j--)
    {
        for (i = 0; i < 100; i++)
        {
            if (wfreq[i])
            {
                if (wfreq[i] <= j)
                    printf("%3s", " ");
                else
                    printf("%3s", "*");
            }
        }
        printf("\n");
    }
    for (i = 0; i < 100; i++)
        if (wfreq[i])
            printf("%3d", (i + 1));
    printf("\n");
    return 0;
}
