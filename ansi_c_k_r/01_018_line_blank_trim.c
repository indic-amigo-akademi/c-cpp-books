#include <stdio.h>
#define MAXLINE 1000 /* maximum input line size */

int getline_(char line[], int maxline);
int blank_strip(char line[], int len);

/* print longest input line */
int main(int argc, char const *argv[])
{
    int len;            /* current line length */
    char line[MAXLINE]; /* current input line */

    while ((len = getline_(line, MAXLINE)) > 0)
        if (blank_strip(line, len) > 0)
            printf("%s", line);

    return 0;
}

/* getline: read a line into s, return length */
int getline_(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    if (c == '\n')
    {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

/* remove blanks and tabs trailing at end and back */
int blank_strip(char line[], int len)
{
    int i, j;
    i = j = 0;
    while (line[i] == ' ' || line[i] == '\t')
        ++i;

    while (i < len)
    {
        line[j++] = line[i++];
        if (line[i] == '\0' || line[i] == '\n')
            break;
    }

    while (j >= 0)
    {
        j--;
        if (line[j] == ' ' || line[j] == '\t')
            line[j] = 0;
        else
        {
            j++;
            break;
        }
    }

    if (j == 0)
        return 0;

    line[j] = '\0';

    return j;
}