#include <stdio.h>

/* count lines in input */
int main(int argc, char const *argv[])
{
    int c, nl = 0;

    while ((c = getchar()) != EOF)
        if (c == '\n')
            ++nl;
    printf("%d\n", nl);
    return 0;
}
