#include <stdio.h>

float fahr_to_celsius(int fahr);

int main(int argc, char const *argv[])
{
    int fahr, celsius;
    int lower, upper, step;

    lower = 0;   /* lower limit of temperature scale */
    upper = 300; /* upper limit */
    step = 20;   /* step size */

    fahr = lower;
    while (fahr <= upper)
    {
        celsius = fahr_to_celsius(fahr);
        printf("%3d\t%6.1d\n", fahr, celsius);
        fahr = fahr + step;
    }
}

float fahr_to_celsius(int fahr)
{
    return 5 * (fahr - 32) / 9;
}