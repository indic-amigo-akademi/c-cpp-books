#include <stdio.h>

/* count newlines, blanks, tabs in input */
int main(int argc, char const *argv[])
{
    int c, nl = 0, nb = 0, nt = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == '\n')
            ++nl;
        if (c == ' ')
            ++nb;
        if (c == '\t')
            ++nt;
    }
    printf("newlines: %d, spaces: %d, tabs: %d\n", nl, nb, nt);
    return 0;
}
