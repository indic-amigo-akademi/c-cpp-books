#include <stdio.h>
#define TABINC 8

/* replace tabs with proper no of blank space to next tab stop */
int main(int argc, char const *argv[])
{
    int nb, pos, c;

    nb = 0;
    pos = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == '\t')
        {
            nb = TABINC - pos % TABINC;
            pos = 0;

            while (nb > 0)
            {
                putchar(' ');
                ++pos;
                --nb;
            }
        }
        else if (c == '\n')
        {
            pos = 0;
            putchar(c);
        }
        else
        {
            putchar(c);
            ++pos;
        }
    }

    return 0;
}
