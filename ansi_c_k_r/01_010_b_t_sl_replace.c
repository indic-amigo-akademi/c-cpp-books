#include <stdio.h>

/* replace each tab by \t, each backspace by \b and
each backslash by \\ */
int main(int argc, char const *argv[])
{
    int c;

    while ((c = getchar()) != EOF)
    {
        if (c == '\\')
        {
            putchar('\\'); putchar('\\');
        }
        else if(c == '\t')
        {
            putchar('\\'); putchar('t');
        }
        else if(c == '\b')
        {
            putchar('\\'); putchar('b');
        }
        else
        {
            putchar(c);
        }
    }
    return 0;
}
