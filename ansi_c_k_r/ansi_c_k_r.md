# ANSI C

## Chapter 1 - A Tutorial Introduction

### 1.1 Print the words

A C program, whatever its size, consists of functions and variables. A function contains statements that specify the computing operations to be done, and variables store values used during the computation.

One method of communicating data between functions is for the calling function to provide a list of values, called arguments, to the function it calls. The parentheses after the function name surround the argument list.

A sequence of characters in double quotes, like "hello, world\n" , is called a **character string** or **string constant**.

An **escape sequence** like \n provides a general and extensible mechanism for representing hard-to-type or invisible characters. Among the others that C provides are \t for tab, \b for backspace, \" for the double quote and \\ for the backslash itself.

### 1.2 Variables and Arithmetic Expressions

A declaration announces the properties of variables; it consists of a name and a list of variables, such as

```c
int fahr, celsius;
int lower, upper, step;
```

C provides several other data types besides int and float , including:
char character - a single byte
short short integer
long long integer
double double-precision floating point.

The size of these objects is also machine-dependent. There are also arrays, structures and unions of these basic types, pointers to them, and functions that return them.

The while loop operates as follows: The condition in parentheses is tested. If it is true ( `fahr` is less than or equal to `upper` ), the body of the loop (the three statements enclosed in braces) is executed. Then the condition is re-tested, and if true, the body is executed again. When the test becomes false ( fahr exceeds upper ) the loop ends, and execution continues at the statement that follows the loop. There are no further statements in this program, so it
terminates.

integer division truncates: any fractional part is discarded.

Each % construction in the first argument of printf is paired with the corresponding second argument, third argument, etc.; they must match up properly by number and type, or wrong answers are obtained.

%d - print as decimal integer
%6d - print as decimal integer, at least 6 characters wide
%f - print as floating point
%6f - print as floating point, at least 6 characters wide
%.2f - print as floating point, 2 characters after decimal point
%6.2f - print as floating point, at least 6 wide and 2 after decimal point

### 1.3 The for statement

Within the parentheses, there are three parts, separated by semicolons. The first part, the initialization `fahr = 0`, condition evaluation `fahr <= 300`; if it is true, the body of the loop (here a single printf ) is executed. Then the increment step `fahr = fahr + 20` is executed, and the condition re-evaluated. The loop terminates if the condition has become false.

### 1.4 Symbolic Constants

A **#define line** defines a symbolic name or symbolic constant to be a particular string of characters:
# define \<name\> \<replacement\> \<list\>

Any occurrence of name (not in quotes and not part of another name) will be
replaced by the corresponding replacement text. The name has the same form as a variable name: a sequence of letters and digits that begins with a letter. The replacement text can be any sequence of characters; it is not limited to numbers.

### 1.5 Character Input and Output

A **text stream** is a sequence of characters divided into lines; each line consists of zero or more characters followed by a newline character.

Each time it is called, `getchar` reads the next input character from a text stream and returns that as its value. `c = getchar();`

The function putchar prints a character each time it is called: `putchar(c)`; prints the contents of the integer variable c as a character, usually on the screen.

#### 1.5.1 File Copying

**EOF** is an integer defined in /<stdio.h/>, but the specific numeric value doesn't matter as long as it is not the same as any char value.

#### 1.5.2 Character Counting

The isolated semicolon, called a **null statement**, is there to satisfy that requirement.

#### 1.5.3 Line Counting

A character written between single quotes represents an integer value equal to the numerical value of the character in the machine's character set is called a **character constant**.

#### 1.5.4 Word Counting

One and only one of the two statements associated with an if-else is performed. If the expression is true, statement 1 is executed; if not, statement 2 is executed. Each statement can be a single statement or several in braces.

### 1.6 Arrays

The conditions are evaluated in order from the top until some condition is satisfied; at that point the corresponding statement part is executed, and the entire construction is finished.

### 1.7 Functions

A function provides a convenient way to encapsulate some computation, which can then be used without worrying about its implementation.

The terms formal argument and actual argument are used for the same distinction.

A function need not return a value; a return statement with no expression causes control, but no useful value, to be returned to the caller, as does "falling off the end" of a function by reaching the terminating right brace. And the calling function can ignore a value returned by a
function.

The declaration

```c
int power(int base, int n);
```

This declaration is called a function prototype. It is an error if the definition of a function or any uses of it do not agree with its prototype.

### 1.8 Arguments - Call by Value

In C, all function arguments are passed by value, means that the called function is given the values of its arguments in temporary variables rather than the originals. It usually leads to more compact programs with fewer extraneous variables, because parameters can be treated as conveniently initialized local variables in the called routine.

When necessary, it is possible to arrange for a function to modify a variable in a calling routine. The caller must provide the address of the variable to be set (technically a pointer to the variable), and the called function must declare the parameter to be a pointer and access the
variable indirectly through it.

### 1.9 Character Arrays

Write a program that reads a set of text lines and prints the longest. The outline is simple enough:
while (there's another line)
if (it's longer than the previous longest)
(save it)
(save its length)
print longest line

### 1.10 External Variables and Scope

An external variable must be defined , exactly once, outside of any function; this sets aside storage for it. The variable must also be declared in each function that wants to access it; this states the type of the variable.

Each local variable in a function comes into existence only when the function is called, and disappears when the function is exited. This is why such variables are usually known as automatic variables, following terminology in other languages.

In certain circumstances, the extern declaration can be omitted. If the definition of the external variable occurs in the source file before its use in a particular function, then there is no need for an extern declaration in the function. The extern declarations in main , getline and copy are thus redundant.

If the program is in several source files, and a variable is defined in file1 and used in file2 and file3 , then extern declarations are needed in file2 and file3 to connect the occurrences of the variable. The usual practice is to collect extern declarations of variables and functions in a separate file, historically called a header , that is included by #include at the front of each source file. The suffix .h is conventional for header names. The functions of the standard library, for example, are declared in headers like \<stdio.h\>.
