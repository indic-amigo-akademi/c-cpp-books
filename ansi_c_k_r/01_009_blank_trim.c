#include <stdio.h>

/* count characters in input; 1st version */
int main(int argc, char const *argv[])
{
    int c, flag = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == ' ')
        {
            if (flag == 0)
                putchar(c);
            flag = 1;
        }
        else
        {
            flag = 0;
            putchar(c);
        }
    }
    return 0;
}
