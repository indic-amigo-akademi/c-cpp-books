#include <stdio.h>
#define MAXLINE 1000 /* maximum input line size */

int getline_(char line[], int maxline);
int reverse(char line[], int len);

int main(int argc, char const *argv[])
{
    int len;            /* current line length */
    char line[MAXLINE]; /* current input line */

    while ((len = getline_(line, MAXLINE)) > 0)
    {
        if (reverse(line, len) > 0)
            printf("%s", line);
    }
    return 0;
}

/* getline: read a line into s, return length */
int getline_(char s[], int lim)
{
    int c, i;

    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    if (c == '\n')
    {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

int reverse(char line[], int len)
{
    int i, j;
    char c;
    for (i = 0, j = len - 1; i < j; i++, j--)
    {
        c = line[i];
        line[i] = line[j];
        line[j] = c;
    }
    return i;
}