#include <stdio.h>

/* print Celsius-Fahrenheit table
for fahr = -150, -130, ..., 150; floating-point version*/

int main(int argc, char const *argv[])
{
    float fahr, celsius;
    int lower, upper, step;

    lower = -150; /* lower limit of temperature scale */
    upper = 150;  /* upper limit */
    step = 20;    /* step size */

    celsius = lower;
    printf("%s\n", "Celsius-Fahrenheit Table");
    printf("%s\t%s\n", "Celsius", "Fahrenheit");
    while (celsius <= upper)
    {
        fahr = 9 * celsius / 5 + 32;
        printf("%8.0f\t%7.1f\n", celsius, fahr);
        celsius = celsius + step;
    }
    return 0;
}